#!/usr/bin/python3
import config
import telegram
import os
import subprocess
import sys
import shlex
import datetime
from subprocess import Popen, PIPE
from telegram.ext import CommandHandler
from imp import reload

#bot = telegram.Bot(token = config.token)
#Проверка бота
#print(bot.getMe())
from telegram.ext import Updater
updater = Updater(token=config.token)
dispatcher = updater.dispatcher


def run_command(command):
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    global textoutput
    textoutput = ''
    while True:
        global output
        output = process.stdout.readline()
        output = output.decode('utf8')
        if output == '' and process.poll() is not None:
            break
        if output:
            print (output.strip())
        textoutput = textoutput + '\n' + output.strip()
    rc = process.poll()
    return rc


def start(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Привет, я бот, жду твои команды. Они указаны в /help.")

# Что бы использовать "lastb" нужно быть root или добавить пользователя
# в /etc/sudoers например такой строчкой
# где test имя пользователя: test  ALL=NOPASSWD:/usr/bin/lastb
def help(bot, update):
    reload(config)
    bot.sendMessage(chat_id=update.message.chat_id, text='''Список доступных команд:
    /id - id пользователя.
    /last - Эти лица удачно вошли на сервер (последние 15).
    /lastb - А эти пытались зайти, но не смогли (последние 15).
    /ipwan - Внешний ip адрес.
    /ipa - Локальные ip адреса без IPv6.
    /whoami - Кто я?
    /hostname - Что за ПК?
    /uptime - Сколько прошло времени с последнего старта.
    /fulluptime - Полный аптайм.
    /date - Текущая дата на ПК.
    /df - Информация о дисковом пространстве (df -h).
    /free - Информация по памяти.
    /mpstat - Информация о нагрузке на процессор.
    ''')

#функция команады id
def myid(bot, update):
    userid = update.message.from_user.id
    bot.sendMessage(chat_id=update.message.chat_id, text=userid)

#функция команады last
def last(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("last -15")
        bot.sendMessage(chat_id=update.message.chat_id, text="Эти пользователи свободно заходили на сервер:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады lastb
# Если от root оставляем как есть, если от пользователя:
# комментируем run_command("lastb -15") и убираем ком-рий с run_command("sudo lastb -15")
def lastb(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("lastb -15")
#        run_command("sudo lastb -15")
        bot.sendMessage(chat_id=update.message.chat_id, text="Эти лица пытались попасть на сервер, но у них не вышло:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады ipwan
def ipwan(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("curl ifconfig.io")
        bot.sendMessage(chat_id=update.message.chat_id, text="Поговаривают нам дали вот этот внешний IP адрес:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады ipa
def ipa(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("hostname -I")
        bot.sendMessage(chat_id=update.message.chat_id, text="Я правда искал и кроме 127.0.0.1 нашёл ещё вот эти IP адреса:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады whoami
def whoami(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("whoami")
        bot.sendMessage(chat_id=update.message.chat_id, text="Судя по всему, на данный момент:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады hostname
def hostname(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("hostname")
        bot.sendMessage(chat_id=update.message.chat_id, text="Все твои команды принимает ПК известный под ником:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады uptime
def uptime(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("uptime -p")
        bot.sendMessage(chat_id=update.message.chat_id, text="Твой сервер работает без остановок уже:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады fulluptime
def fulluptime(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("uptime")
        bot.sendMessage(chat_id=update.message.chat_id, text="Полный аптайм:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады date
def date(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("date")
        bot.sendMessage(chat_id=update.message.chat_id, text="Команда date на данном ПК показывает следующие цифры:")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады df
def df(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("df -h")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады free
def free(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("free -m")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады mpstat
def mpstat(bot, update):
    reload(config)
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("mpstat")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)


start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

help_handler = CommandHandler('help', help)
dispatcher.add_handler(help_handler)

myid_handler = CommandHandler('id', myid)
dispatcher.add_handler(myid_handler)

last_handler = CommandHandler('last', last)
dispatcher.add_handler(last_handler)

lastb_handler = CommandHandler('lastb', lastb)
dispatcher.add_handler(lastb_handler)

ipwan_handler = CommandHandler('ipwan', ipwan)
dispatcher.add_handler(ipwan_handler)

ipa_handler = CommandHandler('ipa', ipa)
dispatcher.add_handler(ipa_handler)

ipa_handler = CommandHandler('whoami', whoami)
dispatcher.add_handler(ipa_handler)

ipa_handler = CommandHandler('hostname', hostname)
dispatcher.add_handler(ipa_handler)

uptime_handler = CommandHandler('uptime', uptime)
dispatcher.add_handler(uptime_handler)

fulluptime_handler = CommandHandler('fulluptime', fulluptime)
dispatcher.add_handler(fulluptime_handler)

date_handler = CommandHandler('date', date)
dispatcher.add_handler(date_handler)

df_handler = CommandHandler('df', df)
dispatcher.add_handler(df_handler)

free_handler = CommandHandler('free', free)
dispatcher.add_handler(free_handler)

mpstat_handler = CommandHandler('mpstat', mpstat)
dispatcher.add_handler(mpstat_handler)

updater.start_polling()
